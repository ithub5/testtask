using System;
using System.Collections.Generic;
using StateSystem.Utils;

namespace StateSystem
{
    public abstract class AStatesController
    {
        public Dictionary<Type, AState> States { get; }

        protected AStatesController()
        {
            States = new Dictionary<Type, AState>();
        }

        public void ModifyActionsInState(Type stateType, StateActionType actionType, bool addRemove,
            params Action<AState>[] actions)
        {
            if (!States.ContainsKey(stateType))
            {
                return;
            }
            
            States[stateType].ModifyActions(actionType, addRemove, actions);
        }
    }
}