using System;
using System.Linq;

namespace StateSystem
{
    public class StateMachine
    {
        private AStatesController _statesController;
        
        public AState CurrentState { get; private set; }

        public StateMachine(AStatesController statesController, Type initState)
        {
            _statesController = statesController;
            
            ChangeState(initState);
        }

        public bool ToState(Type nextState)
        {
            if (CurrentState != null && !CurrentState.NextStates.Contains(nextState))
            {
                return false;
            }
            
            ChangeState(nextState);
            return true;
        }

        
        private void ChangeState(Type type)
        {
            CurrentState?.Exit();
            CurrentState = _statesController.States[type];
            CurrentState.Enter();
        }
    }
}