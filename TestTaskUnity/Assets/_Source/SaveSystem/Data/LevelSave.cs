using LevelSystem.Data;
using UnityEngine;

namespace SaveSystem.Data
{
    [System.Serializable]
    public class LevelSave
    {
        [field: SerializeField] public LevelSettingsSO LevelSettingsSO { get; private set; }
        [field: SerializeField] public string[] ActivePlatformsPrefabNames { get; private set; }
        [field: SerializeField] public Vector3[] ActivePlatformsPositions { get; private set; }
        
        [field: SerializeField] public LevelPoolSave LevelPoolSave { get; private set; }

        public LevelSave(LevelSettingsSO levelSettingsSo, string[] activePlatformsPrefabNames, 
            Vector3[] activePlatformsPositions, LevelPoolSave levelPoolSave)
        {
            LevelSettingsSO = levelSettingsSo;
            ActivePlatformsPrefabNames = activePlatformsPrefabNames;
            ActivePlatformsPositions = activePlatformsPositions;
            LevelPoolSave = levelPoolSave;
        }
    }
}