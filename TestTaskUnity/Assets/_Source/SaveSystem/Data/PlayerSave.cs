using UnityEngine;

namespace SaveSystem.Data
{
    [System.Serializable]
    public class PlayerSave
    {
        [field: SerializeField] public float MoveAnimationTime { get; private set; }
        [field: SerializeField] public float MoveAnimationDirection { get; private set; }

        public PlayerSave(float moveAnimationTime, float moveAnimationDirection)
        {
            MoveAnimationTime = moveAnimationTime;
            MoveAnimationDirection = moveAnimationDirection;
        }
    }
}