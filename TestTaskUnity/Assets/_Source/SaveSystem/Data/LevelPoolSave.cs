using LevelSystem.Data;
using UnityEngine;

namespace SaveSystem.Data
{
    [System.Serializable]
    public class LevelPoolSave
    {
        [field: SerializeField] public PlatformsSO PlatformsSO { get; private set; }
        
        [field: SerializeField] public string[] CurrentPool { get; private set; }
        [field: SerializeField] public string[] SupportPool { get; private set; }
        
        [field: SerializeField] public int ElementsOutCount { get; private set; }

        public LevelPoolSave(PlatformsSO platformsSo, string[] currentPool, string[] supportPool, int elementsOutCount)
        {
            PlatformsSO = platformsSo;
            CurrentPool = currentPool;
            SupportPool = supportPool;
            ElementsOutCount = elementsOutCount;
        }
    }
}