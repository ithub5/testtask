using UnityEngine;

namespace SaveSystem.Data
{
    [CreateAssetMenu(fileName = "Save", menuName = "SO/SaveSystem/Save")]
    public class SaveSO : ScriptableObject
    {
        [field: SerializeField] public PlayerSave PlayerSave { get; private set; }
        [field: SerializeField] public LevelSave LevelSave { get; private set; }
        [field: SerializeField] public int ScoreSave { get; private set; }

        [field: SerializeField] public bool hasSave { get; private set; }

        public void Save(PlayerSave playerSave, LevelSave levelSave, int scoreSave)
        {
            PlayerSave = playerSave;
            LevelSave = levelSave;
            ScoreSave = scoreSave;

            hasSave = true;
        }

        public void Clear()
        {
            PlayerSave = default;
            LevelSave = default;
            ScoreSave = default;

            hasSave = false;
        }
    }
    
}