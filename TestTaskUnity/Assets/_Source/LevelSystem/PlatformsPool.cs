using System.Collections.Generic;
using System.Linq;
using LevelSystem.Data;
using SaveSystem.Data;
using UnityEngine;
using Utils;

namespace LevelSystem
{
    public class PlatformsPool
    {
        private PlatformsSO _platformsSO;
        
        private Transform _spawnPoint;

        private Queue<Platform> _platformsFirstHalf;
        private Queue<Platform> _platformsSecondHalf;

        private Queue<Platform> _currentPool;
        private Queue<Platform> _supportPool;

        private int _onePoolHalfCapacity;
        private int _elementsOutCounter;

        public PlatformsPool(PlatformsSO platformsSO, Transform spawnPoint)
        {
            _platformsSO = platformsSO;

            _spawnPoint = spawnPoint;

            InitPools();
            FullFillPools();
        }
        
        public PlatformsPool(LevelPoolSave save, Transform spawnPoint)
        {
            _platformsSO = save.PlatformsSO;

            _spawnPoint = spawnPoint;

            InitPools();
            FullFillPools(save.CurrentPool, save.SupportPool);
            _elementsOutCounter = save.ElementsOutCount;
        }

        private void InitPools()
        {
            _platformsFirstHalf = new Queue<Platform>();
            _platformsSecondHalf = new Queue<Platform>();
            _onePoolHalfCapacity = _platformsSO.Prefabs.Length * _platformsSO.EachPlatformDuplicates / 2;
            
            _currentPool = _platformsFirstHalf;
            _supportPool = _platformsSecondHalf;
        }
        
        private void FullFillPools()
        {
            for (int i = 0; i < _platformsSO.Prefabs.Length; i++)
            {
                for (int j = 0; j < _platformsSO.EachPlatformDuplicates / 2; j++)
                {
                    Platform newPlatform = Object.Instantiate(_platformsSO.Prefabs[i], _spawnPoint).GetComponent<Platform>();
                    newPlatform.gameObject.SetActive(false);
                    _platformsFirstHalf.Enqueue(newPlatform);
                    
                    newPlatform = Object.Instantiate(_platformsSO.Prefabs[i], _spawnPoint).GetComponent<Platform>();
                    newPlatform.gameObject.SetActive(false);
                    _platformsSecondHalf.Enqueue(newPlatform);
                }
            }
            
            ShufflePool(_platformsFirstHalf);
            ShufflePool(_platformsSecondHalf);
        }
        
        private void FullFillPools(string[] currentPool, string[] supportPool)
        {
            LoadPool(currentPool, _platformsFirstHalf);
            LoadPool(supportPool, _platformsSecondHalf);
            
            ShufflePool(_platformsFirstHalf);
            ShufflePool(_platformsSecondHalf);
        }

        private void LoadPool(string[] save, Queue<Platform> pool)
        {
            for (int i = 0; i < save.Length; i++)
            {
                GameObject newPlatformPrefab = _platformsSO.Prefabs.First(platform => platform.PrefabName == save[i]).gameObject;
                Platform newPlatform = Object.Instantiate(newPlatformPrefab, _spawnPoint).GetComponent<Platform>();
                pool.Enqueue(newPlatform);
            }
        }

        public Platform GetPlatform()
        {
            if (_elementsOutCounter < _onePoolHalfCapacity)
            {
                _elementsOutCounter++;
                return _currentPool.Dequeue();
            }

            _elementsOutCounter++;
            return _supportPool.Dequeue();
        }

        public void ReturnPlatform(Platform platformToReturn)
        {
            if (_currentPool.Count < _onePoolHalfCapacity)
            {
                _currentPool.Enqueue(platformToReturn);
            }
            else if (_currentPool.Count == _onePoolHalfCapacity)
            {
                SwitchPools();
            }
        }

        private void SwitchPools()
        {
            (_currentPool, _supportPool) = (_supportPool, _currentPool);
            _elementsOutCounter -= _onePoolHalfCapacity;
            ShufflePool(_supportPool);
        }
        
        private void ShufflePool(Queue<Platform> pool)
        {
            Platform[] tempPlatforms = pool.Shuffle().ToArray();
            pool.Clear();
            for (int i = 0; i < tempPlatforms.Length; i++)
            {
                pool.Enqueue(tempPlatforms[i]);
            }
        }

        public LevelPoolSave Save()
        {
            string[] currentPool = _currentPool.Select(platform => platform.PrefabName).ToArray();
            string[] supportPool = _supportPool.Select(platform => platform.PrefabName).ToArray();
            return new LevelPoolSave(_platformsSO,  currentPool, supportPool, _elementsOutCounter);
        }
    }
}