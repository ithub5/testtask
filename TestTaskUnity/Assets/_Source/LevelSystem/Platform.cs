using System;
using UnityEngine;

namespace LevelSystem
{
    [RequireComponent(typeof(Rigidbody))]
    public class Platform : MonoBehaviour
    {
        [field: SerializeField] public string PrefabName { get; private set; }
        
        [SerializeField] private Rigidbody rb;
        [SerializeField] private float moveSpeed;

        private bool _isGoing;

        public int HolesCount => transform.childCount;
        
        public event Action<Platform> OnInvisible;

        public void Go()
        {
            SetMovement(true);
        }

        public void Stop()
        {
            SetMovement(false);
        }

        private void SetMovement(bool goStop)
        {
            if (_isGoing == goStop)
            {
                return;
            }

            rb.velocity = goStop ? Vector3.left * moveSpeed : Vector3.zero;
            _isGoing = goStop;
        }

        private void OnBecameInvisible()
        {
            gameObject.SetActive(false);
            OnInvisible?.Invoke(this);
        }
    }
}