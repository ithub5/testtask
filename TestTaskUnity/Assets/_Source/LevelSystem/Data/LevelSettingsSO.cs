using UnityEngine;

namespace LevelSystem.Data
{
    [CreateAssetMenu(fileName = "LevelSettings", menuName = "SO/LevelSystem/Settings")]
    public class LevelSettingsSO : ScriptableObject
    {
        [field: SerializeField] public float SpawnDelay { get; private set; }
    }
}