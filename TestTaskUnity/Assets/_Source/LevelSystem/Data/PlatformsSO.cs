using UnityEngine;

namespace LevelSystem.Data
{
    [CreateAssetMenu(fileName = "LevelPlatforms", menuName = "SO/LevelSystem/Platforms")]
    public class PlatformsSO : ScriptableObject
    {
        [field: SerializeField] public Platform[] Prefabs { get; private set; }
        [field: SerializeField] [field: Range(1, 10)] public int EachPlatformDuplicates { get; private set; }
    }
}