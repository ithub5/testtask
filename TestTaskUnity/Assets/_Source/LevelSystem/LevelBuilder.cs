using System.Collections.Generic;
using System.Linq;
using LevelSystem.Data;
using UnityEngine;
using DG.Tweening;
using SaveSystem.Data;

namespace LevelSystem
{
    public class LevelBuilder : MonoBehaviour
    {
        [SerializeField] private LevelSettingsSO settingsSO;
        [SerializeField] private Transform spawnPosition;
        
        private PlatformsPool _pool;

        private readonly List<Platform> _currentActivePlatforms = new List<Platform>();

        private Sequence _spawnSequence;

        public void Init(PlatformsPool pool)
        {
            _pool = pool;
            InitSequence();
        }

        public void StartLevel()
        {
            _currentActivePlatforms.ForEach(platform => platform.Go());
            _spawnSequence.Play();
        }

        public void StopLevel()
        {
            _currentActivePlatforms.ForEach(platform => platform.Stop());
            _spawnSequence.Pause();
        }

        private void SpawnPlatform()
        {
            Platform currentPlatform = _pool.GetPlatform();
            _currentActivePlatforms.Add(currentPlatform);
            currentPlatform.OnInvisible += ReturnPlatform;

            currentPlatform.transform.position = spawnPosition.position;
            currentPlatform.gameObject.SetActive(true);
            currentPlatform.Go();
        }

        private void ReturnPlatform(Platform platformToReturn)
        {
            platformToReturn.OnInvisible -= ReturnPlatform;
            _currentActivePlatforms.Remove(platformToReturn);
            _pool.ReturnPlatform(platformToReturn);
        }

        private void InitSequence()
        {
            _spawnSequence = DOTween.Sequence();
            _spawnSequence.SetAutoKill(false);

            _spawnSequence.AppendCallback(SpawnPlatform);
            _spawnSequence.AppendInterval(settingsSO.SpawnDelay);
            _spawnSequence.AppendCallback(() => _spawnSequence.Restart());

            _spawnSequence.Pause();
        }

        public LevelSave Save()
        {
            string[] currentPlatformsPrefabs = _currentActivePlatforms.Select(platform => platform.PrefabName).ToArray();
            Vector3[] currentPlatformsPositions = _currentActivePlatforms.Select(platform => platform.transform.position).ToArray();
            return new LevelSave(settingsSO, currentPlatformsPrefabs, currentPlatformsPositions, _pool.Save());
        }

        public void Load(LevelSave save)
        {
            settingsSO = save.LevelSettingsSO;
            
            LoadCurrentPlatforms(save.LevelPoolSave.PlatformsSO.Prefabs, save.ActivePlatformsPrefabNames, save.ActivePlatformsPositions);
        }

        private void LoadCurrentPlatforms(Platform[] platformPrefabs, string[] currentPlatformsPrefabNames, Vector3[] currentPlatformsPositions)
        {
            for (int i = 0; i < currentPlatformsPositions.Length; i++)
            {
                GameObject newPlatformPrefab = platformPrefabs.First(platform => platform.PrefabName == currentPlatformsPrefabNames[i]).gameObject;
                Platform newPlatform = Instantiate(newPlatformPrefab, spawnPosition).GetComponent<Platform>();
                
                newPlatform.transform.position = currentPlatformsPositions[i];
                
                _currentActivePlatforms.Add(newPlatform);
                newPlatform.OnInvisible += ReturnPlatform;
            }
        }
    }
}