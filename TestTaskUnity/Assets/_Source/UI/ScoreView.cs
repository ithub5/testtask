using TMPro;
using UnityEngine;

namespace UI
{
    public class ScoreView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreTexts;

        public void UpdateScore(int newScore)
        {
            scoreTexts.text = newScore.ToString(); 
        }
    }
}