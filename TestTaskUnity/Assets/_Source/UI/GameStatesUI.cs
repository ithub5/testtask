using Core.States;
using StateSystem;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class GameStatesUI : MonoBehaviour
    {
        [field: Header("Waiting")] 
        [field: SerializeField] public GameObject WaitingCanvas { get; private set; }
        [field: SerializeField] public Button PlayBtn { get; private set; }
        
        [field: Header("Play")]
        [field: SerializeField] public GameObject PlayCanvas { get; private set; }
        [field: SerializeField] public Button PauseBtn { get; private set; }
        
        [field: Header("Pause")]
        [field: SerializeField] public GameObject PauseCanvas { get; private set; }
        [field: SerializeField] public Button ResumeBtn { get; private set; }
        [field: SerializeField] public Button QuitBtn { get; private set; }
        
        [field: Header("Finish")]
        [field: SerializeField] public GameObject FinishCanvas { get; private set; }
        [field: SerializeField] public Button RestartBtn { get; private set; }

        public void ShowStateCanvas(AState state)
        {
            OperateStateCanvas(state, true);
        }

        public void HideStateCanvas(AState state)
        {
            OperateStateCanvas(state, false);
        }

        private void OperateStateCanvas(AState state, bool onOff)
        {
            GameObject target = null;
            
            if (state.GetType() == typeof(PlayState))
                target = PlayCanvas;
            else if (state.GetType() == typeof(PauseState))
                target = PauseCanvas;
            else if (state.GetType() == typeof(WaitingState))
                target = WaitingCanvas;
            else if (state.GetType() == typeof(FinishState))
                target = FinishCanvas;

            if (target != null) 
                target.SetActive(onOff);
        }
    }
}