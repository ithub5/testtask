using PlayerSystem;

namespace UI
{
    public class ScoreInvoker
    {
        private ScoreView _view;

        private PlayerPhysics _playerPhysics;

        private int _score;

        public ScoreInvoker(ScoreView view, PlayerPhysics playerPhysics, int score = 0)
        {
            _view = view;

            _playerPhysics = playerPhysics;

            _score = score;
            _view.UpdateScore(_score);
        }

        public void Bind()
        {
            _playerPhysics.OnScore += UpdateScore;
        }

        public void Expose()
        {
            _playerPhysics.OnScore -= UpdateScore;
        }

        private void UpdateScore(int scoreToAdd)
        {
            _score += scoreToAdd;
            _view.UpdateScore(_score);
        }

        public int Save()
        {
            return _score;
        }
    }
}