using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Utils
{
    public static class MyExtensions
    {
        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> collection)
        {
            return collection.OrderBy(item => Random.value);
        }

        public static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            for (int i = 0; i < collection.Count(); i++)
            {
                action(collection.ElementAt(i));
            }
        }
        
        public static bool Contains(this LayerMask mask, int layer)
        {
            return mask == (mask | (1 << layer));
        }
    }
}