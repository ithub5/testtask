using System;
using LevelSystem;
using UnityEngine;
using Utils;

namespace PlayerSystem
{
    public class PlayerPhysics : MonoBehaviour
    {
        [SerializeField] private LayerMask deathLayers;
        [SerializeField] private LayerMask scoreLayers;

        public event Action OnDeath;
        public event Action<int> OnScore;

        private void OnTriggerEnter(Collider other)
        {
            if (deathLayers.Contains(other.gameObject.layer))
            {
                OnDeath?.Invoke();
                return;
            }

            if (scoreLayers.Contains(other.gameObject.layer))
            {
                Platform overcomePlatform = other.gameObject.GetComponent<Platform>();
                OnScore?.Invoke(overcomePlatform.HolesCount);
            }
        }
    }
}