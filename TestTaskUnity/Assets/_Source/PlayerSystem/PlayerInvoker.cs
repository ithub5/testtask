using System;
using InputSystem;
using SaveSystem.Data;
using UnityEngine;
using UnityEngine.InputSystem;

namespace PlayerSystem
{
    public class PlayerInvoker
    {
        private InputHandler _input;

        private PlayerPhysics _physics;
        private PlayerView _view;

        private Vector2 _swipeStartPos;

        public event Action OnDeath;

        public PlayerInvoker(InputHandler input, PlayerPhysics physics, PlayerView view)
        {
            _input = input;

            _physics = physics;
            _view = view;
        }

        public PlayerInvoker(InputHandler input, PlayerPhysics physics, PlayerView view, PlayerSave save) : this(input,
            physics, view)
        {
            _view.Load(save);
        }

        public void Start()
        {
            _view.Go();
            Bind();
        }
        
        public void Stop()
        {
            _view.Stop();
            Expose();
        }

        private void Bind()
        {
            _input.PlayerActions.Press.started += DetectSwipeStart;
            _input.PlayerActions.Press.canceled += DetectSwipeEnd;

            _physics.OnDeath += Die;
        }
        
        private void Expose()
        {
            _input.PlayerActions.Press.started -= DetectSwipeStart; 
            _input.PlayerActions.Press.canceled -= DetectSwipeEnd;

            _physics.OnDeath -= Die;
        }
        
        private void DetectSwipeStart(InputAction.CallbackContext obj)
        {
            _swipeStartPos = _input.PlayerActions.PointerPosition.ReadValue<Vector2>();
        }
        
        private void DetectSwipeEnd(InputAction.CallbackContext ctx)
        {
            Vector2 swipeEndPos = _input.PlayerActions.PointerPosition.ReadValue<Vector2>();
            Vector2 swipeDelta = swipeEndPos - _swipeStartPos;

            if (Mathf.Abs(swipeDelta.y) < 100f)
            {
                return;
            }
            
            bool swipeDirection = swipeDelta.y > 0;
            _view.ChangeDirection(swipeDirection);
        }

        private void Die()
        {
            OnDeath?.Invoke();
        }

        public PlayerSave Save()
        {
            return _view.Save();
        }
    }
}