using SaveSystem.Data;
using UnityEngine;

namespace PlayerSystem
{
    public class PlayerView : MonoBehaviour
    {
        [SerializeField] private Animator animator;

        private bool _currentDirection = true;

        private float _directionHolder = 1f;

        private void Awake()
        {
            Stop();
        }

        /// <param name="upDown">true: up, false: down</param>
        public void ChangeDirection(bool upDown)
        {
            if (_currentDirection == upDown)
            {
                return;
            }
            
            ReverseDirection();
        }

        public void ReverseDirection()
        {
            float newDirection = animator.GetFloat("MoveDirection") * -1f;
            animator.SetFloat("MoveDirection",  newDirection);  
            
            _currentDirection = !_currentDirection;
        }

        public void Go()
        {
            animator.SetFloat("MoveDirection", _directionHolder);
        }
        
        public void Stop()
        {
            _directionHolder = animator.GetFloat("MoveDirection");
            
            animator.SetFloat("MoveDirection",  0f);  
        }

        public void UpdateDirection() =>
            _currentDirection = !_currentDirection;

        public PlayerSave Save()
        {
            float moveAnimationTime = animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            return new PlayerSave(moveAnimationTime, _directionHolder);
        }
        
        public void Load(PlayerSave save)
        {
            animator.SetFloat("MoveDirection", save.MoveAnimationDirection);  
            animator.Play("Move", 0, save.MoveAnimationTime);
        }
    }
}