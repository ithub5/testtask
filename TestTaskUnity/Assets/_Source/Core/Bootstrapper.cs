using LevelSystem;
using LevelSystem.Data;
using PlayerSystem;
using InputSystem;
using SaveSystem.Data;
using UI;
using UnityEngine;

namespace Core
{
    public class Bootstrapper : MonoBehaviour
    {
        [Header("Player")] 
        [SerializeField] private PlayerPhysics playerPhysics;
        [SerializeField] private PlayerView playerView;

        [Header("Level")] 
        [SerializeField] private LevelBuilder levelBuilder;
        [SerializeField] private LevelSettingsSO levelSettingsSO;
        [SerializeField] private PlatformsSO platformsSO;
        [SerializeField] private Transform spawnPoint;

        [Header("UI")] 
        [SerializeField] private GameStatesUI gameStatesUI;

        [Header("Score")]
        [SerializeField] private ScoreView scoreView;

        [Header("Save")] 
        [SerializeField] private SaveSO saveSO;
        
        private Game _game;
        
        private InputHandler _input;
        
        private PlayerInvoker _playerInvoker;
        private PlatformsPool _platformsPool;
        private ScoreInvoker _scoreInvoker;
        
        private void Awake()
        {
            _input = new InputHandler();

            if (!saveSO.hasSave)
            {
                InitWithoutSave();
            }
            else
            {
                InitWithSave();
            }
            
            
            _game = new Game(_playerInvoker, levelBuilder, _scoreInvoker, gameStatesUI, saveSO);
        }

        private void InitWithoutSave()
        {
            _playerInvoker = new PlayerInvoker(_input, playerPhysics, playerView);
            _platformsPool = new PlatformsPool(platformsSO, spawnPoint);
            levelBuilder.Init(_platformsPool);
            _scoreInvoker = new ScoreInvoker(scoreView, playerPhysics);
        }

        private void InitWithSave()
        {
            _playerInvoker = new PlayerInvoker(_input, playerPhysics, playerView, saveSO.PlayerSave);
            _platformsPool = new PlatformsPool(saveSO.LevelSave.LevelPoolSave, spawnPoint);
            levelBuilder.Init(_platformsPool);
            levelBuilder.Load(saveSO.LevelSave);
            _scoreInvoker = new ScoreInvoker(scoreView, playerPhysics, saveSO.ScoreSave);
        }
    }
}