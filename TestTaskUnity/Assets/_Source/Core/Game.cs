using System;
using Core.States;
using LevelSystem;
using PlayerSystem;
using SaveSystem.Data;
using StateSystem;
using StateSystem.Utils;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using PauseState = Core.States.PauseState;

namespace Core
{
    public class Game
    {
        private PlayerInvoker _playerInvoker;
        private LevelBuilder _levelBuilder;
        private ScoreInvoker _scoreInvoker;
        
        private GameStatesController _statesController;
        private StateMachine _stateMachine;
        private GameStatesUI _statesUI;

        private SaveSO _saveSO;

        public Game(PlayerInvoker playerInvoker, LevelBuilder levelBuilder, ScoreInvoker scoreInvoker, GameStatesUI statesUI, SaveSO saveSO)
        {
            _playerInvoker = playerInvoker;
            _levelBuilder = levelBuilder;
            _scoreInvoker = scoreInvoker;
            
            _statesUI = statesUI;
            _statesController = new GameStatesController();
            InitStates();
            _stateMachine = new StateMachine(_statesController, typeof(WaitingState));

            _saveSO = saveSO;
            InitStateUIButtons();
        }

        private void Play(AState state)
        {
            Bind();

            _playerInvoker.Start();
            _levelBuilder.StartLevel();
            _scoreInvoker.Bind();
        }

        private void Pause(AState state)
        {
            Expose();

            _playerInvoker.Stop();
            _levelBuilder.StopLevel();
            _scoreInvoker.Expose();
        }

        private void Bind()
        {
            _playerInvoker.OnDeath += PlayerDeath;
        }

        private void Expose()
        {
            _playerInvoker.OnDeath -= PlayerDeath;
        }
        
        private void InitStates()
        {
            
            _statesController.ModifyActionsInState(typeof(PlayState), StateActionType.enter, true, Play);
            _statesController.ModifyActionsInState(typeof(PlayState), StateActionType.exit, true, Pause);

            SetStateCanvases(typeof(WaitingState));
            SetStateCanvases(typeof(PlayState));
            SetStateCanvases(typeof(PauseState));
            SetStateCanvases(typeof(FinishState));
        }

        private void SetStateCanvases(Type state)
        {
            _statesController.ModifyActionsInState(state, StateActionType.enter, true, _statesUI.ShowStateCanvas);
            _statesController.ModifyActionsInState(state, StateActionType.exit, true, _statesUI.HideStateCanvas);
        }

        private void InitStateUIButtons()
        {
            _statesUI.PlayBtn.onClick.AddListener(() => _stateMachine.ToState(typeof(PlayState)));
            _statesUI.PauseBtn.onClick.AddListener(() => _stateMachine.ToState(typeof(PauseState)));
            _statesUI.QuitBtn.onClick.AddListener(() => { Save(); Application.Quit(); });
            _statesUI.ResumeBtn.onClick.AddListener(() => _stateMachine.ToState(typeof(PlayState)));
            _statesUI.RestartBtn.onClick.AddListener(() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex));
        }
        
        private void PlayerDeath()
        {
            _stateMachine.ToState(typeof(FinishState));
        }

        private void Save()
        {
            _saveSO.Save(_playerInvoker.Save(), _levelBuilder.Save(), _scoreInvoker.Save());
        }
    }
}