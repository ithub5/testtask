using System;
using StateSystem;

namespace Core.States
{
    public class WaitingState : AState
    {
        public WaitingState(params Type[] nextStates) : base(nextStates)
        {
        }
    }
}