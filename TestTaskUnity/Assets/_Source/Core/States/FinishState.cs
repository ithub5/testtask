using System;
using StateSystem;

namespace Core.States
{
    public class FinishState : AState
    {
        public FinishState(params Type[] nextStates) : base(nextStates)
        {
        }
    }
}