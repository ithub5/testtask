using System;
using StateSystem;

namespace Core.States
{
    public class GameStatesController : AStatesController
    {
        public GameStatesController()
        {
            States.Add(typeof(WaitingState), new WaitingState(typeof(PlayState)));
            States.Add(typeof(PlayState), new PlayState(typeof(PauseState), typeof(FinishState)));
            States.Add(typeof(PauseState), new PauseState(typeof(PlayState)));
            States.Add(typeof(FinishState), new FinishState());
        }
    }
}