using System;
using StateSystem;

namespace Core.States
{
    public class PauseState : AState
    {
        public PauseState(params Type[] nextStates) : base(nextStates)
        {
        }
    }
}