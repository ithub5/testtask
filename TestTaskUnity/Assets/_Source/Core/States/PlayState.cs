using System;
using StateSystem;

namespace Core.States
{
    public class PlayState : AState
    {
        public PlayState(params Type[] nextStates) : base(nextStates)
        {
        }
    }
}