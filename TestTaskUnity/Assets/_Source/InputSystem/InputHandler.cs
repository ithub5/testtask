namespace InputSystem
{
    public class InputHandler
    {
        private MainActions _actions;

        public MainActions.PlayerActions PlayerActions => _actions.Player;

        public InputHandler()
        {
            _actions = new MainActions();
        
            Enable();
        }

        public void Enable() =>
            _actions.Enable();
    
        public void Disable() =>
            _actions.Disable();
    }
}
